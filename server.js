const express = require('express')

const todoRoutes = express.Router()
const userRoutes = express.Router()
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const PORT = 4000

let Todo = require('./todo.model');
let User = require('./users.model');

app.use(cors())
app.use(bodyParser.json())
app.use('/todos', todoRoutes)
app.use('/users', userRoutes)

userRoutes.route('/').get(function(req, res) {
    User.find(function(err, users) {
        if (err) {
            console.log(err);
        } else {
            res.json(users);
        }
    });
});

todoRoutes.route('/').get(function(req, res) {
    Todo.find(function(err, todos) {
        if (err) {
            console.log(err);
        } else {
            res.json(todos);
        }
    });
});

todoRoutes.route('/:id').get(function(req, res){
    let id = req.params.id

    Todo.findById(id, function(err, todo){
        res.json(todo);
    })
})

todoRoutes.route('/add').post(function(req, res){
    let todo = new Todo(req.body)
    todo.save()
        .then(todo => {
            res.status(200).json({'todo': 'Added successfully.'})
        })
        .catch(err => {
            res.status(400).send('Adding new todo failed.')
        })
})

userRoutes.route('/register').post(function(req, res){
  let user = new User(req.body)
  user.save()
      .then(user => {
          res.status(200).json({'user': 'Register successfully.'})
      })
      .catch(err => {
          res.status(400).send('Adding new user failed.')
      })
})

userRoutes.route('/login').post(function(req, res){
  const email = req.body.email;
  const password = req.body.password;
  // Find user by email
  User.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
    // Check password
    if (password===user.password) {
      res.status(200).json({user: 'User successfully Login'});

    } else {
      return res
        .status(400)
        .json({ passwordincorrect: "Password incorrect" });
    }
  });
});

todoRoutes.route('/update/:id').post(function(req, res) {
    Todo.findById(req.params.id, function(err, todo) {
        if (!todo)
            res.status(404).send("data is not found");
        else
            todo.todo_description = req.body.todo_description;
            todo.todo_responsible = req.body.todo_responsible;
            todo.todo_priority = req.body.todo_priority;
            todo.todo_completed = req.body.todo_completed;
            todo.save().then(todo => {
                res.json('Todo updated!');
            })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
    });
});

mongoose.connect('mongodb://127.0.0.1:27017/todos', { useNewUrlParser: true });

const connection = mongoose.connection;

connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})

app.listen(PORT, function(){
    console.log("server is running on port " + PORT)
})
